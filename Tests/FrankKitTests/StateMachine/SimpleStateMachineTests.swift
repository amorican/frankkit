//
//  SimpleStateMachineTests.swift
//  FrankKit
//
//  Created by Frank Le Grand on 5/4/17.
//
//

import XCTest
@testable import FrankKit

class SimpleStateMachineTests: XCTestCase {
    
    var machine: SimpleStateMachine<MockMachineDelegate>?
    var delegate: MockMachineDelegate?

    override func setUp() {
        super.setUp()
        let delegate = MockMachineDelegate()
        self.machine = SimpleStateMachine<MockMachineDelegate>(initialState: .ready, delegate: delegate)
        self.delegate = delegate
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_WhenSettingMachineStateToValidState_ThenStateIsChanged() {
        XCTAssert(self.machine?.state == .ready, "The machine state should have been 'ready'.")
        self.machine?.state = .doingSomething
        XCTAssert(self.machine?.state == .doingSomething, "The machine should have transitioned to 'doingSomething'.")
        XCTAssert(self.delegate?.isDoingSomething ?? false, "The delegate should have been doing something as the machine state transitioned.")
    }
    
    func test_WhenSettingMachineStateToInvalidState_ThenStateIsChanged() {
        XCTAssert(self.machine?.state == .ready, "The machine state should have been 'ready'.")
        self.machine?.state = .done("I promise, I'm done. JK.")
        
        XCTAssert(self.machine?.state == .ready, "The machine shouldn't have transitioned out of 'ready'.")
        XCTAssertFalse(self.delegate?.isDoingSomething ?? false, "The delegate shouldn't have been doing something as the machine state didn't transition.")
        XCTAssertNil(self.delegate?.result, "The delegate shouldn't have any result as the machine state didn't transition.")
    }
    
    func test_WhenMachineStateHasTransitioned_ThenDelegateHasPerformedAction() {
        test_WhenSettingMachineStateToValidState_ThenStateIsChanged()
        let someResult = "I am done"
        self.machine?.state = .done(someResult)
        XCTAssert(self.delegate?.result as? String == someResult, "The delegate should show the result.")
    }
    
    static var allTests: [(String, (SimpleStateMachineTests) -> () throws -> Void)] {
        return [
            ("test_WhenSettingMachineStateToValidState_ThenStateIsChanged", test_WhenSettingMachineStateToValidState_ThenStateIsChanged),
            ("test_WhenSettingMachineStateToInvalidState_ThenStateIsChanged", test_WhenSettingMachineStateToInvalidState_ThenStateIsChanged),
            ("test_WhenMachineStateHasTransitioned_ThenDelegateHasPerformedAction", test_WhenMachineStateHasTransitioned_ThenDelegateHasPerformedAction)
        ]
    }
}

// MARK: - Mock Implementation

class MockMachineDelegate: SimpleStateMachineDelegate {
    
    // MARK: SimpleStateMachineDelegate Implementation
    
    enum MockMachineDelegateState: SimpleStateMachineState, Equatable {
        case ready, doingSomething
        case done(Any)
        
        public func canTransition(from: StateType, to: StateType) -> Bool {
            switch (from, to) {
            case (_, .ready):
                return true
            case (.ready, .doingSomething):
                return true
            case (.doingSomething, .done(_)):
                return true
            default:
                return false
            }
        }

        public static func == (left: StateType, right: StateType) -> Bool {
            switch (left, right) {
            case (.ready, .ready):
                return true
            case (.doingSomething, .doingSomething):
                return true
            case (.done, .done):
                return true
            default:
                return false
            }
        }
    }
    
    public typealias StateType = MockMachineDelegateState
    
    func didTransition(from: StateType, to: StateType) {
        
        switch (from, to) {
        case (_, .ready):
            self.getReady()
        case (.ready, .doingSomething):
            self.doSomething()
        case (.doingSomething, .done(let someResult)):
            self.handleDoneDoingSomething(withResult: someResult)
            
        default:
            break
        }
    }
    
    // MARK: Mock Properties
    
    fileprivate(set) var isReady = false
    fileprivate(set) var isDoingSomething = false
    fileprivate(set) var result: Any?
    
    // MARK: Machine functions
    
    func getReady() {
        result = nil
        isDoingSomething = false
        isReady = true
    }
    
    func doSomething() {
        isDoingSomething = true
    }
    
    func handleDoneDoingSomething(withResult result: Any?) {
        isDoingSomething = false
        self.result = result
    }
}
