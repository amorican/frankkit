//
//  PromiseTests.swift
//  FrankKit
//
//  Created by Frank Le Grand on 5/24/17.
//
//

import XCTest
@testable import FrankKit

class PromiseTests: XCTestCase {
    enum TestError: Error {
        case failure
    }
    var fixtureEndpoint = FixtureEndpoint()
    
    override func setUp() {
        super.setUp()
        fixtureEndpoint = FixtureEndpoint()
        self.continueAfterFailure = false
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    func testClosureExecution() {
        let expectation = self.expectation(description: "testSuccess")
        Promise<Bool> { _ in
                expectation.fulfill()
            }.finally()
        self.wait(for: [expectation], timeout: 1.0)
    }
    func testClosureDoesNotExecute() {
        _ = Promise<Bool> { completion in
                XCTFail("Closure should not have been executed")
                completion(.failure(TestError.failure))
            }//.finally()
    }

    func testSuccess() {
        let expectation = self.expectation(description: "testSuccess")
        Promise<Bool> { completion in
            DispatchQueue.global().async {
                RunLoop.current.run(until: Date.init(timeIntervalSinceNow: TimeInterval(0.25)))
                completion(.success(true))
            }
            }.finally(
                error: { (error: Error) in
                    XCTFail("\(error)")
            },
                success: { (result: Bool) in
                    expectation.fulfill()
                    XCTAssertTrue(result, "Result should have been true")
            })
        self.wait(for: [expectation], timeout: 1.0)
    }
    func testError() {
        let expectation = self.expectation(description: "testError")
        Promise<Bool> { completion in
            DispatchQueue.global().async {
                RunLoop.current.run(until: Date.init(timeIntervalSinceNow: TimeInterval(0.25)))
                completion(.failure(TestError.failure))
            }
            }.finally(
                error: { (_: Error) in
                    expectation.fulfill()
            },
                success: { (_: Bool) in
                    XCTFail("Succcess closure shouldn't have been called")
            })
        self.wait(for: [expectation], timeout: 1.0)
    }

    func testMap() {
        let expectation = self.expectation(description: "testMap")
        Promise<Bool> { completion in
            DispatchQueue.global().async {
                RunLoop.current.run(until: Date.init(timeIntervalSinceNow: TimeInterval(0.25)))
                completion(.success(true))
            }
            }.map { (_: Bool) -> String in
                return "mapped value"
            }.finally(
                error: { (error: Error) in
                    XCTFail("\(error)")
            },
                success: { (result: String) in
                    expectation.fulfill()
                    XCTAssertEqual(result, "mapped value")
            })
        self.wait(for: [expectation], timeout: 1.0)
    }
    func testMap_error() {

        let expectation = self.expectation(description: "testMap_error")
        Promise<Bool> { completion in
            DispatchQueue.global().async {
                RunLoop.current.run(until: Date.init(timeIntervalSinceNow: TimeInterval(0.25)))
                completion(.success(true))
            }
            }.map { (_: Bool) -> String in
                throw TestError.failure
            }.finally(
                error: { (_: Error) in
                    expectation.fulfill()
            },
                success: { (_: String) in
                    XCTFail("Succcess closure shouldn't have been called")
            })
        self.wait(for: [expectation], timeout: 1.0)
    }
    func testMap_fromFailure() {
        let expectation = self.expectation(description: "testMap_fromFailure")
        Promise<Bool> { completion in
                DispatchQueue.global().async {
                    RunLoop.current.run(until: Date.init(timeIntervalSinceNow: TimeInterval(0.25)))
                    completion(.failure(TestError.failure))
                }
            }.map { (_: Bool) -> String in
                XCTFail("Promise failing should have skipped mapping")
                return "mapped value"
            }
            .finally(
                error: { (_: Error) in
                    expectation.fulfill()
            },
                success: { (_: String) in
                    XCTFail("Succcess closure shouldn't have been called")
            })
        self.wait(for: [expectation], timeout: 1.0)
    }

    func testFlatMap() {
        let expectation = self.expectation(description: "testFlatMap")
        Promise<Bool> { completion in
                DispatchQueue.global().async {
                    RunLoop.current.run(until: Date.init(timeIntervalSinceNow: TimeInterval(0.25)))
                    completion(.success(true))
                }
            }.flatMap { (_: Bool) -> Promise<String> in
                return Promise { completion in
                    DispatchQueue.global().async {
                        RunLoop.current.run(until: Date.init(timeIntervalSinceNow: TimeInterval(0.25)))
                        completion(.success("mapped value"))
                    }
                }
            }.finally(
                error: { (error: Error) in
                    XCTFail("\(error)")
            },
                success: { (result: String) in
                    expectation.fulfill()
                    XCTAssertEqual(result, "mapped value")
            })
        self.wait(for: [expectation], timeout: 1.0)
    }
    func testFlatMap_failure() {
        let expectation = self.expectation(description: "testFlatMap_failure")
        Promise<Bool> { completion in
            DispatchQueue.global().async {
                RunLoop.current.run(until: Date.init(timeIntervalSinceNow: TimeInterval(0.25)))
                completion(.success(true))
            }
            }.flatMap { (_: Bool) -> Promise<String> in
                return Promise { completion in
                    DispatchQueue.global().async {
                        RunLoop.current.run(until: Date.init(timeIntervalSinceNow: TimeInterval(0.25)))
                        completion(.failure(TestError.failure))
                    }
                }
            }.finally(
                error: { (_: Error) in
                    expectation.fulfill()
            },
                success: { (_: String) in
                    XCTFail("flatMap should have failed")
            })
        self.wait(for: [expectation], timeout: 1.0)
    }
    func testFlatMap_fromFailure() {
        let expectation = self.expectation(description: "testFlatMap_fromFailure")
        Promise<Bool> { completion in
            DispatchQueue.global().async {
                RunLoop.current.run(until: Date.init(timeIntervalSinceNow: TimeInterval(0.25)))
                completion(.failure(TestError.failure))
            }
            }.flatMap { (_: Bool) -> Promise<String> in
                return Promise { completion in
                    DispatchQueue.global().async {
                        RunLoop.current.run(until: Date.init(timeIntervalSinceNow: TimeInterval(0.25)))
                        completion(.success("mapped value"))
                    }
                }
            }.finally(
                error: { (_: Error) in
                    expectation.fulfill()
            },
                success: { (_: String) in
                    XCTFail("flatMap should have failed")
            })
        self.wait(for: [expectation], timeout: 1.0)
    }

    func testAsyncWait() {
        // TODO
//        do {
//            let object = try ModelableFixture.compose()
//            let representation = object.originalRepresentation
//            fixtureEndpoint.fixtureResults.append(.success(RestResult.init(value: RestResult.ResultType.object(representation))))
//
//            let results: RestResult = try ModelableFixture.query().run().asyncWait()
//
//            XCTAssertEqual(results.read?.count, 1)
//        } catch let e {
//            XCTFail(String(describing: e))
//        }
    }
    func testAsyncWait_error() {
//        do {
//            fixtureEndpoint.fixtureResults.append(.failure(PromiseError.executionFailure))
//
//            let _: ModelableResult = try ParentFixture.query().run().asyncWait()
//
//            XCTFail("An error should have been thrown")
//        } catch _ {
//
//        }
    }
    func testBeginAsync() {
//        var results = ModelableResult()
//        let expectation = self.expectation(description: "Background action")
//        //perform on background
//        beginAsync {
//            do {
//                let parent = try ParentFixture.compose()
//                self.fixtureEndpoint.fixtureResults.append(.success(ModelableResult(read: [parent])))
//                results = try ParentFixture.query().run().asyncWait()
//            } catch let e {
//                XCTFail(String(describing: e))
//            }
//            expectation.fulfill()
//        }
//
//        XCTAssertNil(results.read)//before background starts, results should be 0
//        self.wait(for: [expectation], timeout: 1.0)//wait for background
//        XCTAssertEqual(results.read?.count, 1)//after background runs, we should have 1 item
    }

    static var allTests: [(String, (PromiseTests) -> () throws -> Void)] {
        return [
            ("testClosureExecution", testClosureExecution),
            ("testClosureDoesNotExecute", testClosureDoesNotExecute),

            ("testSuccess", testSuccess),
            ("testError", testError),

            ("testMap", testMap),
            ("testMap_error", testMap_error),
            ("testMap_fromFailure", testMap_fromFailure)
        ]
    }

}
