//
//  ResultTests.swift
//  FrankKit
//
//  Created by Frank Le Grand on 3/27/17.
//

import XCTest
import Foundation
@testable import FrankKit

#if os(Linux)
public func random_int() -> Int {
    return random()
}
#else
public func random_int() -> Int {
    return Int(arc4random())
}
#endif

public enum ResultError: Error, CustomStringConvertible {
    case standardError(message: String?)

    public var description: String {
        switch self {
        case .standardError(let message):
            return message ?? ""
        }
    }
}

class ResultTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSuccess() {

        let result: Result = .success(random_int())

        XCTAssertTrue(result.isSuccess)
    }
    func testFailure() {

        let result: Result<Int> = .failure(ResultError.standardError(message: nil))

        XCTAssertTrue(result.isFailure)
    }
    func testValue_success() {
        let value = random_int()

        let result: Result = .success(value)

        XCTAssertEqual(value, result.value)
    }
    func testValue_failure() {

        let result: Result<Int> = .failure(ResultError.standardError(message: nil))

        XCTAssertNil(result.value)
    }
    func testError_success() {

        let result: Result = .success(random_int())

        XCTAssertNil(result.error)
    }
    func testError_failure() {

        let result: Result<Int> = .failure(ResultError.standardError(message: nil))

        XCTAssertNotNil(result.error)
    }

    static var allTests: [(String, (ResultTests) -> () throws -> Void)] {
        return [
            ("testSuccess", testSuccess),
            ("testFailure", testFailure),
            ("testValue_success", testValue_success),
            ("testValue_failure", testValue_failure),
            ("testError_success", testError_success),
            ("testError_failure", testError_failure)
        ]
    }
}
