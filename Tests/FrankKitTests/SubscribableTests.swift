//
//  SubscribableTests.swift
//  SIKit
//
//  Created by Frank Le Grand on 3/6/17.
//

import XCTest
import Dispatch
import Foundation
@testable import FrankKit

// swiftlint:disable nesting
class SubscribableTests: XCTestCase {

    func testSubscribe() {
        let disposeBag = DisposeBag()
        struct ExampleStruct {
            var property: Subscribable<Int>

            init() {
                property = Subscribable(3)
            }
        }
        let example = ExampleStruct()
        var actionWasCalled = false
        example.property.subscribe(self) { (_, _) in
            actionWasCalled = true
        }.addTo(disposeBag: disposeBag)
        let newValue = 4

        example.property.value = newValue

        XCTAssertTrue(actionWasCalled, "Action handler was never called")
        XCTAssertEqual(example.property.value, newValue)
    }
    func testUnsubscribe() {
        let disposeBag = DisposeBag()
        struct ExampleStruct {
            var property: Subscribable<Int>

            init() {
                property = Subscribable(3)
            }
        }
        class Subscriber {
        }
        //run the code in a closure so that when we are done, subscriber gets deinited
        let example = ExampleStruct()
        let subscriber = Subscriber()
        example.property.subscribe(subscriber) { (_, _) in
            XCTFail("Subscriber action should not be called after subscriber is nil")
            }.addTo(disposeBag: disposeBag)
        example.property.unsubscribe(subscriber)
        let newValue = 4

        example.property.value = newValue

        XCTAssertEqual(example.property.value, newValue)
    }

    func testSubscribe_onQueue() {
        let disposeBag = DisposeBag()
        struct ExampleStruct {
            var property: Subscribable<Int>

            init() {
                property = Subscribable(3)
            }
        }
        let example = ExampleStruct()
        var actionWasCalled = false
        example.property.subscribe(self, on: DispatchQueue.global(qos: .background)) { (_, _) in
            actionWasCalled = true
        }.addTo(disposeBag: disposeBag)
        let newValue = 4

        example.property.value = newValue

        RunLoop.current.run(until: Date.init(timeIntervalSinceNow: TimeInterval(0.25)))//give it a sec to fire up
        XCTAssertTrue(actionWasCalled, "Action handler was never called")
        XCTAssertEqual(example.property.value, newValue)
    }

    static var allTests: [(String, (SubscribableTests) -> () throws -> Void)] {
        return [
            ("testSubscribe", testSubscribe),
            ("testUnsubscribe", testUnsubscribe),
            ("testSubscribe_onQueue", testSubscribe_onQueue)
        ]
    }
}
// swiftlint:enable nesting
