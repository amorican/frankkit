import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(FrankKitTests.allTests),
        testCase(ResultTests.allTests),
        testCase(PromiseTests.allTests),
        testCase(SubscribableTests.allTests),
        testCase(SimpleStateMachineTests.allTests)
    ]
}
#endif
