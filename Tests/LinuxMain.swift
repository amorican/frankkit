import XCTest

import FrankKitTests

var tests = [XCTestCaseEntry]()
tests += FrankKitTests.allTests()
XCTMain(tests)
