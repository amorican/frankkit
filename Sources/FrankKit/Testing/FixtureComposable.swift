//
//  FixtureComposable.swift
//  FranKKit
//
//  Created by Frank Le Grand on 9/11/19.
//

import Foundation

//compose a value from a type like UUID().uuidString for String type
public protocol Composable {
    static func compose(_ defaultValues: JSON?) throws -> Self
}

// MARK: String
extension String: Composable {
    public static func compose(_ defaultValues: JSON? = nil) -> String {
        let str = UUID().uuidString
        return str
    }
}
// MARK: Bool
extension Bool: Composable {
    public static func compose(_ defaultValues: JSON? = nil) -> Bool {
        return true
    }
}
// MARK: Int
extension Int: Composable {
    public static func compose(_ defaultValues: JSON? = nil) -> Int {
        #if os(Linux)
        return random()
        #else
        return Int(arc4random_uniform(2147483647))//max mysql int
        #endif
    }
}
// MARK: Double
extension Double: Composable {
    public static func compose(_ defaultValues: JSON? = nil) -> Double {
        #if os(Linux)
        return Double(random()) * 0.5
        #else
        return Double(arc4random()) * 0.5
        #endif
    }
}
// MARK: Date
extension Date: Composable {
    public static func compose(_ defaultValues: JSON? = nil) -> Date {
        return Date()
    }
}
// MARK: Data
extension Data: Composable {
    public static func compose(_ defaultValues: JSON? = nil) -> Data {
        return Data()
    }
}

// MARK: Array
extension Array: Composable {
    
    public static func compose(_ defaultValues: [String: Any]? = nil) -> [Element] {
        if let type = Element.self as? Composable.Type,
            let item = try? type.compose(defaultValues),
            let elements = [item] as? [Element] {
            return elements
        }
        return []
    }
}

public enum FixtureError: Error, CustomStringConvertible {
    case invalidConfiguration(String)
    public var description: String {
        switch self {
        case .invalidConfiguration(let rawValue):
            return "Error. Skipping property (\(rawValue)). Was it an array of child objects which are the same Type as the parent? ie: Subscriber Zones for a Location Zone. We aren't composing those yet."
        }
    }
}
