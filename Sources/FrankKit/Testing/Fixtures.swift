//
//  Fixtures.swift
//  FranKKit
//
//  Created by Frank Le Grand on 9/11/19.
//

import Foundation

public struct ModelableFixture: Modelable {
    
    let id: String
    let name: String
    let date: Date
    let double: Double
    let data: Data
    let deleted: Bool
    
    let originalRepresentation: JSON
    
    public init(with representation: JSON) throws {
        self.originalRepresentation = representation
        
        self.id = try representation.decode("id")
        self.name = try representation.decode("name")
        self.date = try representation.decode("date")
        self.double = try representation.decode("double")
        self.data = try representation.decode("data")
        self.deleted = try representation.decode("deleted")
    }
}

extension ModelableFixture {
    
    public static let path = "dummy/fixture"
    
    public var primaryId: String { return id }
    public var stringValue: String { return "ModelableFixture \(id) - \(name)" }
    
    public struct Schema {
        public static let id = ModelProperty("id", type: String.self)
        public static let name = ModelProperty("name", type: String.self)
        public static let date = ModelProperty("date", type: Date.self)
        public static let double = ModelProperty("double", type: Double.self)
        public static let data = ModelProperty("data", type: Data.self)
        public static let deleted = ModelProperty("deleted", type: Bool.self)
    }

    public static var properties: [String: ModelProperty] {
        return [
            Schema.id.rawValue: Schema.id,
            Schema.name.rawValue: Schema.name,
            Schema.date.rawValue: Schema.date,
            Schema.double.rawValue: Schema.double,
            Schema.data.rawValue: Schema.data,
            Schema.deleted.rawValue: Schema.deleted
        ]
    }
}
