//
//  FixtureEndpoint.swift
//  FranKKit
//
//  Created by Frank Le Grand on 9/11/19.
//

import Foundation

public class FixtureEndpoint: RestEndpoint {
    
    public let rootURL: String = "https://dummyurl"
    public let headers = [String: String]()

    public var fixtureResults = [Result<RestResult>]()
    private let accessQueue = DispatchQueue(label: "FixtureEndpoint_AccessQueue", attributes: [])
    
    public init(){}
    
    public func get() -> Promise<RestResult> {
        return Promise { completion in
            var result = Result<RestResult>.success(RestResult(value: .object(JSON())))
            self.accessQueue.sync {
                result = self.fixtureResults.first ?? Result<RestResult>.success(RestResult(value: .object(JSON())))
            }
            guard !result.isFailure else {
                completion(result)
                return
            }
            
            let resultValue = result.value!
            let returnResult: Result<RestResult> = result.isSuccess ? Result.success(resultValue) : result
            completion(returnResult)
        }
    }
}
