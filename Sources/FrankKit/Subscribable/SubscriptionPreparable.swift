//
//  SubscriptionPreparable.swift
//  FrankKit
//
//  Created by Frank Le Grand on 9/12/19.
//

import Foundation

public protocol SubscriptionPreparable {
    func prepareSubscriptions()
    func cleanupSubscriptions()
}
