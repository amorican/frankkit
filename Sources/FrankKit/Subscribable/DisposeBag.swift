//
//  DisposeBag.swift
//  SIKit
//
//  Created by Frank Le Grand on 3/3/17.
//

import Foundation

public protocol Disposable {
    func dispose()
}
public final class DisposeBag {

    var items = [Disposable]()

    public init() {}
    public func add(disposable: Disposable) {
        items.append(disposable)
    }
    deinit {
        items.forEach { $0.dispose() }
    }
}
