//
//  Subscribable.swift
//  SIKit
//
//  Created by Frank Le Grand on 3/6/17.
//

import Foundation
import Dispatch

public protocol SubscribableProtocol {
    associatedtype ValueType
    var value: ValueType { get set }
    func subscribe(_ subscriber: AnyObject?,
                   block: @escaping (_ newValue: ValueType, _ oldValue: ValueType) -> Void) -> Subscribable<ValueType>
    func unsubscribe(_ subscriber: AnyObject) -> Subscribable<ValueType>
}

open class Subscribable<T: Any>: SubscribableProtocol, Disposable {
    public typealias SubscriberBlock = (_ oldValue: T, _ newValue: T) -> Void
    public typealias SubscribersEntry = (subscriber: AnyObject?, queue: DispatchQueue?, block: SubscriberBlock)
    
    open var subscribers: [SubscribersEntry]
    
    public init(_ value: T) {
        subscribers = []
        self.value = value
        
    }
    open var value: T {
        didSet {
            notifySubscribers(currentValue: self.value, oldValue: oldValue)
        }
    }
    open func notifySubscribers(currentValue: T, oldValue: T) {
        subscribers.forEach { (entry: SubscribersEntry) in
            let (_, queue, block) = entry
            if let dispatchQueue = queue,
                (dispatchQueue != DispatchQueue.main || !Thread.isMainThread) {
                dispatchQueue.async {
                    block(oldValue, currentValue)
                }
            } else {
                block(oldValue, currentValue)
            }
        }
    }
    
    @discardableResult
    public func subscribe(_ subscriber: AnyObject? = nil, block: @escaping SubscriberBlock) -> Subscribable {
        let entry: SubscribersEntry = (subscriber: subscriber, queue: nil, block: block)
        subscribers.append(entry)
        return self
    }
    @discardableResult
    open func subscribe(_ subscriber: AnyObject? = nil,
                        on dispatchQueue: DispatchQueue,
                        block: @escaping SubscriberBlock) -> Subscribable {
        let entry: SubscribersEntry = (subscriber: subscriber, queue: dispatchQueue, block: block)
        subscribers.append(entry)
        return self
    }
    @discardableResult
    public func unsubscribe(_ subscriber: AnyObject) -> Subscribable {
        let filtered = subscribers.filter { entry in
            let (owner, _, _) = entry
            return owner !== subscriber
        }
        
        subscribers = filtered
        return self
    }
    
    @discardableResult
    open func unretainedSubscribe<Subscriber: Hashable>(_ subscriber: Subscriber,
                                                        on dispatchQueue: DispatchQueue = DispatchQueue.main,
                                                        block: @escaping SubscriberBlock) -> Subscribable {
        let entry: SubscribersEntry = (subscriber: "\(subscriber.hashValue)" as NSString,
                                       queue: dispatchQueue,
                                       block: block)
        subscribers.append(entry)
        return self
    }
    @discardableResult
    public func unretainedUnsubscribe<Subscriber: Hashable>(_ subscriber: Subscriber) -> Subscribable {
        let object = "\(subscriber.hashValue)" as NSString
        let filtered = subscribers.filter { entry in
            let (owner, _, _) = entry
            if let owner = owner {
                let ownerString = NSString(string: String(describing: owner))
                return !ownerString.isEqual(to: "\(object)")
            }
            return true
        }

        subscribers = filtered
        return self
    }
    
    @discardableResult
    public func addTo(disposeBag: DisposeBag) -> Subscribable {
        disposeBag.add(disposable: self)
        return self
    }
    
    public func dispose() {
        subscribers = []
    }
}
