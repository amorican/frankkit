//
//  Promise.swift
//  FrankKit
//
//  Created by Frank Le Grand on 5/20/17.
//
//

import Foundation

public enum PromiseError: Error, CustomStringConvertible {
    case executionFailure
    case mainThread
    case timeout
    
    public var description: String {
        switch self {
        case .executionFailure:
            return "Programmer error. Result wasn't retrieved, most likely the async code didn't run."
        case .mainThread:
            return "Programmer error. You can't call asyncWait on the main thread. It will block."
        case .timeout:
            return "The semaphore timed out."
        }
    }
}

public func beginAsync(_ action: @escaping () -> Void) {
    DispatchQueue.global().async(execute: action)
}
public func onMainThread(_ action: @escaping () -> Void) {
    if Thread.isMainThread {
        action()
    } else {
        DispatchQueue.main.async(execute: action)
    }
}

public struct Promise<T> {
    public typealias ResultType = Result<T>

    private let closure: (@escaping (ResultType) -> Void) -> Void

    public init(closure: @escaping (@escaping (ResultType) -> Void) -> Void) {
        self.closure = closure
    }
    
    public func asyncWait(allowOnMain: Bool = false, timeout: DispatchTime = DispatchTime.now() + DispatchTimeInterval.seconds(30)) throws -> T {
        if Thread.isMainThread && NSClassFromString("XCTestCase") == nil && !allowOnMain {
            throw PromiseError.mainThread
        }
        switch run(timeout: timeout) {
        case .success(let value):
            return value
        case .failure(let error):
            throw error
        }
    }
    
    private func run(timeout: DispatchTime = DispatchTime.now() + DispatchTimeInterval.seconds(10)) -> ResultType {
        var value: ResultType?
        let semaphore = DispatchSemaphore(value: 0)

        self.performClosure { result in
            value = result
            semaphore.signal()
        }

        let timeoutResult = semaphore.wait(timeout: timeout)
        if timeoutResult == .timedOut {
            return Result.failure(PromiseError.timeout)
        }
        if let theValue = value {
            return theValue
        }

        return Result.failure(PromiseError.executionFailure)
    }

    fileprivate func performClosure(_ completion: @escaping (ResultType) -> Void) {
        self.closure { result in
            completion(result)
        }
    }
}
