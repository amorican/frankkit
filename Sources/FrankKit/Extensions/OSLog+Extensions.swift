//
//  OSLog+Extensions.swift
//  FrankKit
//
//  Created by Frank Le Grand on 9/12/19.
//

import Foundation
import os.log

import os.log

public enum FrankLeGrandBundleIdentifier: String {
    case frankKit = "FrankKit"
    
    static var identifierStrings: [String] {
        return [FrankLeGrandBundleIdentifier.frankKit.rawValue]
    }
}

extension OSLog {
    private static var subsystem = FrankLeGrandBundleIdentifier.frankKit.rawValue
    static let frankKit = OSLog(subsystem: subsystem, category: "FrankKit")
}
