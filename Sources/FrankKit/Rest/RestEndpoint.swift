//
//  RestEndpoint.swift
//  FrankKit
//
//  Created by Frank Le Grand on 9/12/19.
//

import Foundation
import os.log

public enum RestEndpointError: Swift.Error {
    case invalidURL(String)
    case invalidRunRequest(String)
    case invalidResponse(String)
}

public enum HttpMethod: String {
    case get = "GET"
    case put = "PUT"
}

public protocol RestEndpoint {
    var rootURL: String { get }
    var headers: [String: String] { get }
}

extension RestEndpoint {
    
    public func run(path: String, method: HttpMethod = .get, postObject: JSON? = nil) -> Promise<RestResult> {
        return Promise { (completion: @escaping (Result<RestResult>) -> Void) in
            let url: URL
            do {
                if method == .get && postObject != nil {
                    completion(.failure(RestEndpointError.invalidRunRequest("Implementation error: A post object may not be provided when submitting a GET request.")))
                    return
                }
                let urlString = NSString(string: self.rootURL).appendingPathComponent(path)
                guard let theURL = URL(string: urlString) else {
                    completion(.failure(RestEndpointError.invalidURL(urlString)))
                    return
                }
                
                url = theURL
            }
            
            let request = NSMutableURLRequest(url: url,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)

            let httpMethodString = method.rawValue
            request.httpMethod = httpMethodString
            request.allHTTPHeaderFields = self.headers
            
            if method == .put, let json = postObject {
                do {
                    let postData = try JSONSerialization.data(withJSONObject: json, options: [])
                    request.httpBody = postData as Data
                }
                catch {
                    completion(.failure(RestEndpointError.invalidRunRequest("Unable to serialize provided post object.")))
                    return
                }
            }

            
            let session = URLSession.shared
            let startDate = Date()
            
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                let endDate = Date()
                let ellapsed = endDate.timeIntervalSince(startDate).stringValue()
                var logMessage = "Completed \(httpMethodString) request - URL = " + String(describing: url) + " - Ellapsed: " + ellapsed
                
                let logInfo = {
                    os_log("%@", log: OSLog.frankKit, type: OSLogType.default, logMessage)
                }
                
                if let error = error {
                    logMessage += "\nERROR: " + String(describing: error)
                    logInfo()
                    completion(.failure(error))
                    return
                }
                else {
                    if let httpResponse = response as? HTTPURLResponse {
                        logMessage += "\nHTTP " + String(describing: httpResponse.statusCode)
                    }
                    else {
                        logMessage += "\nWARNING: Missing HTTP Response"
                    }
                    
                    var responseString = ""
                    if let responseData = data {
                        responseString = String(decoding: responseData, as: UTF8.self)
                    }
                    logMessage += " - JSON Response: " + (responseString.isEmpty ? "nil" : responseString)

                    guard let responseData = data,
                        let jsonObject = try? JSONSerialization.jsonObject(with: responseData, options: []) as? JSON else {
                            if data != nil && data!.count == 0 { completion(.success(RestResult(value: .object(JSON())))) } //empty response is ok, some calls have no data to return
                            else {
                                logMessage += " - ERROR: Unable to serialize response"
                                completion(.failure(JSON.Error.invalidResponse(data)))
                            }
                            logInfo()
                            return
                    }
                    
                    logInfo()
                    completion(.success(RestResult(value: .object(jsonObject))))
                    
                }
            })
            
            let logMessage = "Sending \(httpMethodString) request - URL = " + String(describing: url)
            os_log("%@", log: OSLog.frankKit, type: OSLogType.default, logMessage)
            dataTask.resume()
        }
    }
}
