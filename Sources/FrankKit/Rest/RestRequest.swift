//
//  RestRequest.swift
//  FrankKit
//
//  Created by Frank Le Grand on 9/12/19.
//

import Foundation

public struct RestRequest {
    
    public enum RequestType {
        case get(String) // get with a path
        case put(String, JSON?) // put with a path and a post body
    }
    
    let endpoint: RestEndpoint
    let type: RequestType
}

extension RestRequest {
    
    public func run() -> Promise<RestResult> {
        return Promise { (completion: @escaping (Result<RestResult>) -> Void) in
            beginAsync {
                do {
                    let method: HttpMethod
                    let urlPath: String
                    let body: JSON?
                    
                    switch self.type {
                    case .get(let path):
                        method = .get
                        urlPath = path
                        body = nil
                    case .put(let path, let postObject):
                        method = .put
                        urlPath = path
                        body = postObject
                    }

                    let result = try self.endpoint.run(path: urlPath, method: method, postObject: body).asyncWait()
                    completion(.success(result))
                }
                catch { completion(.failure(error)) }
            }
        }
    }
}
