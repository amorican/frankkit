//
//  JSON.swift
//  FrankKit
//
//  Created by Frank Le Grand on 9/12/19.
//

import Foundation

public typealias JSON = [String: Any]

extension JSON {
    
    public enum Error: Swift.Error {
        case invalidKey(String)
        case invalidValueTypeForKey(String)
        case invalidResponse(Data?)
    }
    
    public func decode<T: Composable>(_ key: String) throws -> T {
        if T.self == Date.self, let value = try decodeDate(key) as? T {
            return value
        }
        guard let value = self[key] as? T else {
            throw Error.invalidKey(key)
        }
        return value
    }

    fileprivate func decodeDate(_ key: String) throws -> Date {
        guard let value = self[key] as? Int else {
            throw Error.invalidValueTypeForKey(key)
        }
        let date = Date(timeIntervalSince1970: TimeInterval(value))
        return date
    }
    
    public func decodeArray<T: Modelable>(_ key: String) throws -> [T] {
        guard let jsonArray = self[key] as? [JSON] else {
            throw Error.invalidValueTypeForKey(key)
        }
        
        let values = try jsonArray.map { try T.init(with: $0) }
        return values
    }

    public func decodeOptional<T: Composable>(_ key: String) throws -> T? {
        guard let value = self[key] else { return nil }
        guard let typedValue = value as? T else { throw Error.invalidValueTypeForKey(key) }
        return typedValue
    }
}
