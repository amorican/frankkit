//
//  RestResult.swift
//  FranKKit
//
//  Created by Frank Le Grand on 9/11/19.
//

import Foundation

public struct RestResult {
    
    public enum ResultType {
        case object(JSON)
        case array([JSON])
    }
    
    public let value: ResultType
    
    public var objectValue: JSON? {
        switch value {
        case .object(let json): return json
        case .array(let array): return array.first
        }
    }
    
    public var arrayValue: [JSON] {
        switch value {
        case .object(let json): return [json]
        case .array(let array): return array
        }
    }
}
