//
//  Modelable.swift
//  FranKKit
//
//  Created by Frank Le Grand on 9/11/19.
//

import Foundation

public protocol Modelable: Composable {
    
    static var path: String { get }
    static var properties: [String: ModelProperty] { get }
    var primaryId: String { get }
    var stringValue: String { get }
    
    init(with representation: JSON) throws
}


extension Modelable {
    
    public static func compose(_ defaultValues: JSON?) throws -> Self {
        var representation = JSON()
        for (name, property) in Self.properties {
            representation[name] = try property.type.compose(defaultValues)
        }
        let model = try Self.init(with: representation)
        return model
    }
}

extension Modelable {

    public static func getRequest(pathParams: String? = nil, endpoint: RestEndpoint) -> RestRequest {
        var path = NSString(string: Self.path)
        if let params = pathParams { path = NSString(string: path).appendingPathComponent(params) as NSString }
        let request = RestRequest(endpoint: endpoint, type: RestRequest.RequestType.get(String(path)))
        return request
    }
    
    public static func putRequest(pathParams: String? = nil, postObject: JSON? = nil, endpoint: RestEndpoint) -> RestRequest {
        var path = NSString(string: Self.path)
        if let params = pathParams { path = NSString(string: path).appendingPathComponent(params) as NSString }
        let request = RestRequest(endpoint: endpoint, type: RestRequest.RequestType.put(String(path), postObject))
        return request
    }
    
    public static func read(pathParams: String?, endpoint: RestEndpoint) -> Promise<Self> {
        return Promise { (completion: @escaping (Result<Self>) -> Void) in
            do {
                let request = self.getRequest(pathParams: pathParams, endpoint: endpoint)
                let result = try request.run().asyncWait()
                let representation = result.objectValue ?? [:]
                let model = try Self.init(with: representation)
                completion(.success(model))
            }
            catch { completion(.failure(error)) }
        }
    }
}
