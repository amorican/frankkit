//
//  ModelProperty.swift
//  FrankKit
//
//  Created by Frank Le Grand on 9/12/19.
//

import Foundation

public protocol PropertyType {
    var rawValue: String { get }
    var type: Composable.Type { get } //a property that we can generate a random value for
}

public struct ModelProperty: PropertyType {
    public let rawValue: String
    public var type: Composable.Type
    public let optional: Bool
    
    public init(_ rawValue: String, type: Composable.Type, optional: Bool = false) {
        self.rawValue = rawValue
        self.type = type
        self.optional = optional
    }
}
