//
//  SimpleStateMachine.swift
//  FrankKit
//
//  Created by Frank Le Grand on 5/4/17.
//

import Foundation

/**
 The state of SimpleStateMachine machine object is represented by an object which must conform to the protocol SimpleStateMachineState.
 Example:
 ```
 enum dataLoaderState {
    case ready, loading
    case success(Data)
    case failure(Error)
 }
 ```
 */
public protocol SimpleStateMachineState {
    
    /**
     The transition rules from one state to another are defined in the enum itself through the implementation of this method.
     - Parameter from: The state from which the machine wants to transition.
     - Parameter to: The state to which the machine proposes to transition.
     - Returns: A `Bool` indicating whether the machine is allowed to switch states.
     */
    func canTransition(from: Self, to: Self) -> Bool
}

/**
 A SimpleStateMachine's delegate defines the states of the machine, the transition rules between states, and gets notified when the machine state's has changed thru `didTransition(from:StateType, to:StateType)`.
 */
public protocol SimpleStateMachineDelegate: class {
    associatedtype StateType: SimpleStateMachineState
    func didTransition(from:StateType, to:StateType)
}

/**
 A SimpleStateMachine object exposes a thread safe state property and notifies its delegate when it changes.
 */
public class SimpleStateMachine<T: SimpleStateMachineDelegate> {
    
    private let accessQueue = DispatchQueue(label: "StateMachine_SimpleStateMachineAccess", attributes: [])
    
    private weak var delegate: T?
    
    private var _state: T.StateType {
        didSet {
            delegate?.didTransition(from: oldValue, to: _state)
        }
    }
    
    public var state: T.StateType {
        get {
            return _state
        }
        set {
            self.accessQueue.sync {
                if self._state.canTransition(from: self._state, to: newValue) {
                    self._state = newValue
                }
            }
        }
    }
    
    public init(initialState: T.StateType, delegate: T) {
        _state = initialState
        self.delegate = delegate
    }
}
